package pl.belluu;

import java.io.IOException;

import com.slack.api.methods.SlackApiException;

public class App {
    public static void main(String[] args) throws IOException, SlackApiException {

        AppConfig appConfig = new AppConfig();

        appConfig.configurationSlack();
    }
}
