package pl.belluu;

import java.io.IOException;

import com.slack.api.Slack;
import com.slack.api.methods.MethodsClient;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.request.chat.ChatPostMessageRequest;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;

public class AppConfig {

    protected void configurationSlack() throws IOException, SlackApiException {

    Slack slack = Slack.getInstance();

    String token = "my_token";
   // String token = System.getenv("my_token");

    MethodsClient methods = slack.methods(token);

    ChatPostMessageRequest request = ChatPostMessageRequest.builder()
    .channel("#random")
    .text(":wave: Hello from bot!")
    .build();

    ChatPostMessageResponse response = methods.chatPostMessage(request);
    }
}